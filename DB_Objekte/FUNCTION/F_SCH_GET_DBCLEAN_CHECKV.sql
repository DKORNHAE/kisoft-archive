CREATE OR REPLACE FUNCTION F_SCH_GET_DBCLEAN_CHECKV 
(
  V_TABNAME IN VARCHAR2 
) RETURN NUMBER AS 

  V_CHECKVALUEDEC   NUMBER(5);
  v_Logging         VARCHAR2(2000); --Variable f�rs Logging
  
BEGIN

  v_Logging := ' TIMESTAMP: '||sqltools.gettimestamp||'TABNAME: '||V_TABNAME;
  
  select checkvaluedec
    into V_CHECKVALUEDEC
  from dbcleancheck
  where tabname = V_TABNAME;

RETURN V_CHECKVALUEDEC;

EXCEPTION 
  WHEN OTHERS THEN
  log_in_db.logerror('ERR',SQLERRM, 'F_SCH_GET_DBCLEAN_CHECKV', v_Logging);
NULL;

END F_SCH_GET_DBCLEAN_CHECKV;