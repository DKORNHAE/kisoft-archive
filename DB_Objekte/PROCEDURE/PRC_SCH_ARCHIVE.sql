create or replace PROCEDURE PRC_SCH_ARCHIVE AS 

V_CHECKVALUEDEC_KUBESTPROT      NUMBER(5);
V_CHECKVALUEDEC_KUBESTPPROT     NUMBER(5);
V_CHECKVALUEDEC_KOMMPROT        NUMBER(5);
V_CHECKVALUEDEC_KOPROT          NUMBER(5);
v_Logging                       VARCHAR2(2000); --Variable f�rs Logging

BEGIN

  v_Logging := ' TIMESTAMP: '||sqltools.gettimestamp;
  
/* 
  Value aus DBCLEAN f�r Archivierung holen
*/
select f_sch_get_dbclean_checkv('KUBESTPROT')-7
    , f_sch_get_dbclean_checkv('KUBESTPPROT')-7
    , f_sch_get_dbclean_checkv('KOMMPROT')-7
    , f_sch_get_dbclean_checkv('KOPROT')-7
into V_CHECKVALUEDEC_KUBESTPROT
    , V_CHECKVALUEDEC_KUBESTPROT
    , V_CHECKVALUEDEC_KOMMPROT
    , V_CHECKVALUEDEC_KOPROT
from dual;



/* 
  Insert in KUBESTARCHIV, KUBESTPARCHIV, KOMMARCHIV, KOARCHIV aus PROT-Tabellen
*/
insert into KUBESTARCHIV
select sqltools.gettimestamp as ARCHIVSTAMP,
       klient.nr,
      kubestprot.*
from kubestprot, 
    partner klient,
    partner kunde
where kubestprot.kundenid = kunde.partnerid
  and kunde.klient = klient.partnerid
  and to_date(substr(kubestprot.protstamp, 1, 8), 'YYYY.MM.DD') < to_date(to_char(sysdate-153, 'YYYY.MM.DD'), 'YYYY.MM.DD')
  and kubestprot.protstamp not in 
    (select kubestarchiv.protstamp from kubestarchiv);
    
  
insert into KUBESTPARCHIV
select sqltools.gettimestamp as ARCHIVSTAMP,
      artikel.artnr,
      artikel.bez1,
      artikel.var,
      klient.nr,
      bundle.be1menge,
      bundle.hebez,
      kubestpprot.*
from kubestpprot, 
    artikel,
    bundle,
    partner klient
where kubestpprot.bundle = bundle.bundleid
  and bundle.artid = artikel.artid
  and artikel.klient = klient.partnerid
  and to_date(substr(kubestpprot.protstamp, 1, 8), 'YYYY.MM.DD') < to_date(to_char(sysdate-153, 'YYYY.MM.DD'), 'YYYY.MM.DD')
  and kubestpprot.protstamp not in 
    (select kubestparchiv.protstamp from kubestparchiv);
    
  
insert into KOMMARCHIV
select sqltools.gettimestamp as ARCHIVSTAMP,
      kommprot.*
from kommprot
where to_date(substr(kommprot.protstamp, 1, 8), 'YYYY.MM.DD') < to_date(to_char(sysdate-153, 'YYYY.MM.DD'), 'YYYY.MM.DD')
  and kommprot.protstamp not in 
    (select kommarchiv.protstamp from kommarchiv);
    

insert into KOARCHIV
select sqltools.gettimestamp as ARCHIVSTAMP,
      koprot.*
from koprot
where to_date(substr(koprot.protstamp, 1, 8), 'YYYY.MM.DD') < to_date(to_char(sysdate-153, 'YYYY.MM.DD'), 'YYYY.MM.DD')
  and koprot.protstamp not in 
    (select koarchiv.protstamp from koarchiv);


EXCEPTION 
  WHEN OTHERS THEN
  log_in_db.logerror('ERR',SQLERRM, 'PRC_SCH_ARCHIVE', v_Logging);
NULL;

END PRC_SCH_ARCHIVE;